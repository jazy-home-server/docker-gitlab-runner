#!/bin/sh
VOLUME=gitlab_runner_data
BACKUP_TAR=test
docker volume create $VOLUME
docker run --rm -v $VOLUME:/gitlab-runner -v $(pwd)/backup/$BACKUP_TAR:/backup/$BACKUP_TAR busybox /bin/sh -c "cd /gitlab-runner && tar xf /backup/$BACKUP_TAR ."

