#!/bin/sh
VOLUME="gitlab_runner_data"
BACKUP_TAR="gitlab-runner.$(date +%s).tar"
docker run --rm -v $VOLUME:/gitlab-runner -v $(pwd)/backup:/backup busybox:1.31 /bin/sh -c "cd /gitlab-runner && tar cf /backup/$BACKUP_TAR ."

